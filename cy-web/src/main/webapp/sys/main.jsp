<%--
  Created by chenyi
  email: qq228112142@qq.com
  Date : 2017-08-08 17:35:59
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/common/jsp/resource.jsp" %>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend> 框架说明</legend>
</fieldset>
<blockquote class="layui-elem-quote">
    1.基于ssm+shiro安全框架的后台管理框架，权限简单易用可控制到按钮。<br>
    2.配置代码生成器，减少70%开发时间，专注业务逻辑。<br>
    3.前端声明式组件封装、附带文档编写 ctrl+c ctrl+v 即可使用。封装数据源，可通过url、枚举、字典直接渲染组件。代码量极少且易维护。<br>
    4.layui常用代码的二次封装，省略layui部分繁琐的代码！&nbsp;&nbsp;&nbsp;<a class="a-link" target="_blank" href="http://www.layui.com">》》》layui官网入口</a><br>
</blockquote>

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend> 如何交流、反馈、参与贡献？</legend>
</fieldset>
<blockquote class="layui-elem-quote">
    1.项目主页 <a class="a-link" target="_blank" href="http://admin.cymall.xin">http://admin.cymall.xin</a><br>
    2.码云地址 <a class="a-link" target="_blank" href="https://gitee.com/leiyuxi/cy-security">https://gitee.com/leiyuxi/cy-security</a><br>
    3.qq群 275846351<br>
</blockquote>

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend> 打赏作者</legend>
</fieldset>
<div style="height:200px;">
    <div class="layui-upload-list" style="display: inline-block;width:200px">
        <div class="file-div">
            <img class="layui-upload-img" src="/statics/img/cy/weixin.jpg" >
        </div>
    </div>
    <div class="layui-upload-list" style="display: inline-block;width:200px">
        <div class="file-div">
            <img class="layui-upload-img" src="/statics/img/cy/zhifubao.jpg"  >
        </div>
    </div>
    <%--<img src="/statics/img/cy/weixin.jpg" style="height:200px">--%>
    <%--<img src="/statics/img/cy/zhifubao.jpg" style="height:200px;margin-left:20px">--%>
</div>

</body>
</html>